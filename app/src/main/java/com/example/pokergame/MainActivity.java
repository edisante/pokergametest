package com.example.pokergame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pokergame.models.Card;
import com.example.pokergame.models.PokerGame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This activity is the entry point for App PokerGame
 * @author Edwin Di Sante <edisante@gmail.com>
 */
public class MainActivity extends AppCompatActivity {

    //TextViews for showing resume game
    private TextView tvPairs;
    private TextView tvThreeOfKing;
    private TextView tvStraight;
    private TextView tvPoker;
    private TextView tvColor;

    //Class core of game
    private PokerGame pokerGame;
    //List for random cards (5 cards)
    private List<Card> cardList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialization
        tvPairs = (TextView) findViewById(R.id.tvPairs);
        tvThreeOfKing = (TextView) findViewById(R.id.tvThreeOfKing);
        tvStraight = (TextView) findViewById(R.id.tvStraight);
        tvPoker = (TextView) findViewById(R.id.tvPoker);
        tvColor = (TextView) findViewById(R.id.tvColor);

        pokerGame = new PokerGame();

        //Button for start new game
        Button btnNewCard = (Button) findViewById(R.id.btnNewCards);

        init();

        //Button click event
        btnNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Create new game and compute statistics
                if (newGame()) {
                    computeStatistics();
                }
            }
        });

    }

    /**
     * Get a copy of cardList
     * @return List<Card>
     */
    public List<Card> getCardList() {

        List<Card> cloneList = new ArrayList<>();

        Iterator<Card> iterator = cardList.iterator();

        while (iterator.hasNext()) {

            try {
                cloneList.add((Card) iterator.next().clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

        return cloneList;

    }

    /**
     * This method calculate all possible hands defined for this game and show the results in the resume
     */
    private void computeStatistics() {

        //Get pair number
        int pairs = pokerGame.getPairs(getCardList());
        if (pairs > 0) {
            tvPairs.setText(String.format("%s %s", getString(R.string.pairs), "" + pairs));
        }
        //Check if there is a Three of King hand
        if (pokerGame.hasThreeOfKing(getCardList())) {
            tvThreeOfKing.setText(String.format("%s %s", getString(R.string.three_of_king), getString(R.string.yes)));
        }
        //Check if there is a Straight hand
        if (pokerGame.hasStraight(getCardList())) {
            tvStraight.setText(String.format("%s %s", getString(R.string.straight), getString(R.string.yes)));
        }
        //Check if there is a Poker hand
        if (pokerGame.hasPoker(getCardList())) {
            tvPoker.setText(String.format("%s %s", getString(R.string.poker), getString(R.string.yes)));
        }
        //Check if there is a Flush or Color hand
        if (pokerGame.hasFlush(getCardList())) {
            tvColor.setText(String.format("%s %s", getString(R.string.color), getString(R.string.yes)));
        }
    }

    /**
     * This method get 5 random cards and shows them
     *
     * @return
     */
    private boolean newGame() {

        cardList = pokerGame.getRandomCards();

        init();

        if (cardList != null && cardList.size() > 0) {

            ImageView ivCard1 = (ImageView) findViewById(R.id.ivCard1);
            ImageView ivCard2 = (ImageView) findViewById(R.id.ivCard2);
            ImageView ivCard3 = (ImageView) findViewById(R.id.ivCard3);
            ImageView ivCard4 = (ImageView) findViewById(R.id.ivCard4);
            ImageView ivCard5 = (ImageView) findViewById(R.id.ivCard5);

            int i = 0;
            ivCard1.setImageDrawable(getDrawable(cardList.get(i++).getIconRes()));
            ivCard2.setImageDrawable(getDrawable(cardList.get(i++).getIconRes()));
            ivCard3.setImageDrawable(getDrawable(cardList.get(i++).getIconRes()));
            ivCard4.setImageDrawable(getDrawable(cardList.get(i++).getIconRes()));
            ivCard5.setImageDrawable(getDrawable(cardList.get(i).getIconRes()));

            return true;

        } else {

            Toast.makeText(getBaseContext(), getString(R.string.error_loading_cards), Toast.LENGTH_LONG).show();
        }

        return false;
    }

    //Init the TextViews
    private void init() {

        tvPairs.setText(String.format("%s %s", getString(R.string.pairs), getString(R.string.not)));
        tvThreeOfKing.setText(String.format("%s %s", getString(R.string.three_of_king), getString(R.string.not)));
        tvStraight.setText(String.format("%s %s", getString(R.string.straight), getString(R.string.not)));
        tvPoker.setText(String.format("%s %s", getString(R.string.poker), getString(R.string.not)));
        tvColor.setText(String.format("%s %s", getString(R.string.color), getString(R.string.not)));

    }
}
