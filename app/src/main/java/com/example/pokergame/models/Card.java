package com.example.pokergame.models;

import java.util.Comparator;

/**
 * This class is the card model
 * @author Edwin Di Sante <edisante@gmail.com>
 */
public class Card implements Cloneable {

    //Value of card
    private int value;
    private int color;
    private String name;
    private int iconRes;

    public Card() {
    }

    public Card(int value, int color, String name, int iconRes) {
        this.value = value;
        this.color = color;
        this.name = name;
        this.iconRes = iconRes;
    }

    /*Comparator for sorting the list by value asc*/
    public static Comparator<Card> CardValueComparatorAsc = new Comparator<Card>() {
        @Override
        public int compare(Card o1, Card o2) {
            return o1.getValue() - o2.getValue();
        }
    };

    @Override
    public Object clone() throws CloneNotSupportedException {
        Card clone = null;

        clone = (Card) super.clone();

        return clone;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }
}
