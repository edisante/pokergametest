package com.example.pokergame.models;

import com.example.pokergame.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * This class is the game core and has the methods for the logic of the game
 * @author Edwin Di Sante <edisante@gmail.com>
 */
public class PokerGame {

    //Full card list
    private List<Card> cardList;

    //Color constants
    private static final int COLOR_RED = 1;
    private static final int COLOR_BLACK = 2;

    //Init all cards of the game
    public PokerGame() {

        cardList = new ArrayList<>();

        cardList.add(new Card(2, COLOR_BLACK, "2_of_clubs", R.drawable.c2_of_clubs));
        cardList.add(new Card(2, COLOR_RED, "2_of_diamonds", R.drawable.c2_of_diamonds));
        cardList.add(new Card(2, COLOR_RED, "2_of_hearts", R.drawable.c2_of_hearts));
        cardList.add(new Card(2, COLOR_BLACK, "2_of_spades", R.drawable.c2_of_spades));

        cardList.add(new Card(3, COLOR_BLACK, "3_of_clubs", R.drawable.c3_of_clubs));
        cardList.add(new Card(3, COLOR_RED, "3_of_diamonds", R.drawable.c3_of_diamonds));
        cardList.add(new Card(3, COLOR_RED, "3_of_hearts", R.drawable.c3_of_hearts));
        cardList.add(new Card(3, COLOR_BLACK, "3_of_spades", R.drawable.c3_of_spades));

        cardList.add(new Card(4, COLOR_BLACK, "4_of_clubs", R.drawable.c4_of_clubs));
        cardList.add(new Card(4, COLOR_RED, "4_of_diamonds", R.drawable.c4_of_diamonds));
        cardList.add(new Card(4, COLOR_RED, "4_of_hearts", R.drawable.c4_of_hearts));
        cardList.add(new Card(4, COLOR_BLACK, "4_of_spades", R.drawable.c4_of_spades));

        cardList.add(new Card(5, COLOR_BLACK, "5_of_clubs", R.drawable.c5_of_clubs));
        cardList.add(new Card(5, COLOR_RED, "5_of_diamonds", R.drawable.c5_of_diamonds));
        cardList.add(new Card(5, COLOR_RED, "5_of_hearts", R.drawable.c5_of_hearts));
        cardList.add(new Card(5, COLOR_BLACK, "5_of_spades", R.drawable.c5_of_spades));

        cardList.add(new Card(6, COLOR_BLACK, "6_of_clubs", R.drawable.c6_of_clubs));
        cardList.add(new Card(6, COLOR_RED, "6_of_diamonds", R.drawable.c6_of_diamonds));
        cardList.add(new Card(6, COLOR_RED, "6_of_hearts", R.drawable.c6_of_hearts));
        cardList.add(new Card(6, COLOR_BLACK, "6_of_spades", R.drawable.c6_of_spades));

        cardList.add(new Card(7, COLOR_BLACK, "7_of_clubs", R.drawable.c7_of_clubs));
        cardList.add(new Card(7, COLOR_RED, "7_of_diamonds", R.drawable.c7_of_diamonds));
        cardList.add(new Card(7, COLOR_RED, "7_of_hearts", R.drawable.c7_of_hearts));
        cardList.add(new Card(7, COLOR_BLACK, "7_of_spades", R.drawable.c7_of_spades));

        cardList.add(new Card(8, COLOR_BLACK, "8_of_clubs", R.drawable.c8_of_clubs));
        cardList.add(new Card(8, COLOR_RED, "8_of_diamonds", R.drawable.c8_of_diamonds));
        cardList.add(new Card(8, COLOR_RED, "8_of_hearts", R.drawable.c8_of_hearts));
        cardList.add(new Card(8, COLOR_BLACK, "8_of_spades", R.drawable.c8_of_spades));

        cardList.add(new Card(9, COLOR_BLACK, "9_of_clubs", R.drawable.c9_of_clubs));
        cardList.add(new Card(9, COLOR_RED, "9_of_diamonds", R.drawable.c9_of_diamonds));
        cardList.add(new Card(9, COLOR_RED, "9_of_hearts", R.drawable.c9_of_hearts));
        cardList.add(new Card(9, COLOR_BLACK, "9_of_spades", R.drawable.c9_of_spades));

        cardList.add(new Card(10, COLOR_BLACK, "10_of_clubs", R.drawable.c10_of_clubs));
        cardList.add(new Card(10, COLOR_RED, "10_of_diamonds", R.drawable.c10_of_diamonds));
        cardList.add(new Card(10, COLOR_RED, "10_of_hearts", R.drawable.c10_of_hearts));
        cardList.add(new Card(10, COLOR_BLACK, "10_of_spades", R.drawable.c10_of_spades));

        cardList.add(new Card(11, COLOR_BLACK, "jack_of_clubs2", R.drawable.jack_of_clubs2));
        cardList.add(new Card(11, COLOR_RED, "jack_of_diamonds2", R.drawable.jack_of_diamonds2));
        cardList.add(new Card(11, COLOR_RED, "jack_of_hearts2", R.drawable.jack_of_hearts2));
        cardList.add(new Card(11, COLOR_BLACK, "jack_of_hearts2", R.drawable.jack_of_hearts2));

        cardList.add(new Card(12, COLOR_BLACK, "queen_of_clubs2", R.drawable.queen_of_clubs2));
        cardList.add(new Card(12, COLOR_RED, "queen_of_diamonds2", R.drawable.queen_of_diamonds2));
        cardList.add(new Card(12, COLOR_RED, "queen_of_hearts2", R.drawable.queen_of_hearts2));
        cardList.add(new Card(12, COLOR_BLACK, "queen_of_spades2", R.drawable.queen_of_spades2));

        cardList.add(new Card(13, COLOR_BLACK, "king_of_clubs2", R.drawable.king_of_clubs2));
        cardList.add(new Card(13, COLOR_RED, "king_of_diamonds2", R.drawable.king_of_diamonds2));
        cardList.add(new Card(13, COLOR_RED, "king_of_hearts2", R.drawable.king_of_hearts2));
        cardList.add(new Card(13, COLOR_BLACK, "king_of_spades2", R.drawable.king_of_spades2));

        cardList.add(new Card(14, COLOR_BLACK, "ace_of_clubs", R.drawable.ace_of_clubs));
        cardList.add(new Card(14, COLOR_RED, "ace_of_diamonds", R.drawable.ace_of_diamonds));
        cardList.add(new Card(14, COLOR_RED, "ace_of_hearts", R.drawable.ace_of_hearts));
        cardList.add(new Card(14, COLOR_BLACK, "ace_of_spades", R.drawable.ace_of_spades));

    }

    /**
     * Get a copy of cardList
     *
     * @return
     * @throws CloneNotSupportedException
     */
    public List<Card> getCardList() throws CloneNotSupportedException {

        List<Card> cloneList = new ArrayList<>();

        Iterator<Card> iterator = cardList.iterator();

        while (iterator.hasNext()) {

            cloneList.add((Card) iterator.next().clone());
        }

        return cloneList;
    }

    /**
     * Return a list of 5 random cards
     *
     * @return List<Card>
     */
    public List<Card> getRandomCards() {

        List<Card> randomCards = new ArrayList<>();
        List<Card> tmpCardList = null;

        try {
            tmpCardList = getCardList();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        Random random = new Random();

        for (int i = 0; i < 5; i++) {

            assert tmpCardList != null;
            if (!tmpCardList.isEmpty()) {
                int randomIndex = random.nextInt(tmpCardList.size());

                randomCards.add(tmpCardList.get(randomIndex));

                tmpCardList.remove(randomIndex);
            }
        }

        return randomCards;
    }


    /**
     * Check if exists a Straight hand
     *
     * @param cardList
     * @return
     */
    public boolean hasStraight(List<Card> cardList) {

        if (cardList != null && cardList.size() > 1) {

            //Sort cardList asc by value
            Collections.sort(cardList, Card.CardValueComparatorAsc);

            //I extract the first item from the list
            Card card = cardList.get(0);
            cardList.remove(0);

            boolean sameSuit = true;
            for (int i = 0; i < cardList.size(); i++) {

                if (cardList.get(i).getValue() != card.getValue() + 1) {
                    return false;
                }
                if (cardList.get(i).getColor() != card.getColor()) {
                    sameSuit = false;
                }
                card = cardList.get(i);
            }

            return !sameSuit;
        }

        return false;
    }


    /**
     * Check if exists a Flush hand
     *
     * @param cardList
     * @return
     */
    public boolean hasFlush(List<Card> cardList) {

        if (cardList != null && cardList.size() > 1) {

            Card card = cardList.get(0);
            cardList.remove(0);

            //Check if all the cards have the same color
            for (int i = 0; i < cardList.size(); i++) {
                if (cardList.get(i).getColor() != card.getColor()) {
                    return false;
                }
            }

        } else {
            return false;
        }

        return true;
    }


    /**
     * Check if exists a Poker hand
     *
     * @param cardList
     * @return
     */
    public boolean hasPoker(List<Card> cardList) {

        if (cardList != null && cardList.size() > 1) {

            int iterator = 0;
            while (iterator < 2) {

                Card card = cardList.get(0);
                cardList.remove(0);

                int matches = 0;
                //Check matches
                for (int i = 0; i < cardList.size(); i++) {
                    if (cardList.get(i).getValue() == card.getValue()) {
                        matches++;
                    }

                }

                //If you find more that 4 cards with the same value is not Poker
                if (matches > 3) {
                    return false;
                }

                //If you find 4 cards with the same value
                if (matches == 3) {
                    return true;
                }

                iterator++;
            }
        }

        return false;
    }

    /**
     * Check if exists a Three of King hand
     *
     * @param cardList
     * @return
     */
    public boolean hasThreeOfKing(List<Card> cardList) {

        if (cardList != null && cardList.size() > 1) {

            int x = 0;
            while (x < cardList.size()) {

                Card card = cardList.get(x++);

                int matches = 0;
                List<Integer> index = new ArrayList<>();

                //Check matches and save index
                for (int i = 0; i < cardList.size(); i++) {
                    if (cardList.get(i).getValue() == card.getValue()) {
                        matches++;
                        index.add(i);
                    }
                }

                //If get 3 card with same value and 2 different is Three of King
                if (matches == 3) {

                    List<Card> tmpList = new ArrayList<>();
                    for (int i = 0; i < cardList.size(); i++) {
                        if (!index.contains(i)) {
                            tmpList.add(cardList.get(i));
                        }
                    }

                    if (tmpList.size() > 1 && tmpList.get(0).getValue() != tmpList.get(1).getValue()) {
                        return true;
                    }

                }
            }
        }

        return false;
    }


    /**
     * Find the number of pairs in the hand
     *
     * @param cardList
     * @return
     */
    public int getPairs(List<Card> cardList) {

        if (cardList != null && cardList.size() > 1) {

            int pair = 0;
            while (cardList.size() > 1) {

                Card card = cardList.get(0);
                cardList.remove(0);

                int matches = 0;
                List<Integer> index = new ArrayList<>();
                for (int i = 0; i < cardList.size(); i++) {
                    if (cardList.get(i).getValue() == card.getValue()) {
                        matches++;
                        index.add(i);
                    }
                }

                if (matches == 1) {
                    pair++;
                }

                List<Card> tmpList = new ArrayList<>();
                for (int i = 0; i < cardList.size(); i++) {
                    if (!index.contains(i)) {
                        tmpList.add(cardList.get(i));
                    }
                }

                cardList = tmpList;

            }

            return pair;
        }

        return 0;
    }
}
